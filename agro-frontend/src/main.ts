import './assets/main.css'

import { createApp } from 'vue'
import App from './App.vue'
import router from './router'

import Socketio from '../plugins/socket.io'

import { ApolloClient, createHttpLink, InMemoryCache } from '@apollo/client/core'
import { createApolloProvider } from '@vue/apollo-option'
import { DefaultApolloClient } from '@vue/apollo-composable'
// import VueApolloComponents from '@vue/apollo-components'

const httpLink = createHttpLink({
  uri: 'http://localhost:6000/graphql'
})

const cache = new InMemoryCache()

const apolloClient = new ApolloClient({
  link: httpLink,
  cache
})

const apolloProvider = createApolloProvider({
  defaultClient: apolloClient
})

const app = createApp(App)

// app.use(VueApolloComponents)

app.use(apolloProvider)

app.provide(DefaultApolloClient, apolloClient)

app.use(Socketio, {
  connection: 'http://localhost:6000',
  options: {
    autoConnect: true
  }
})

app.use(router)

app.mount('#app')
