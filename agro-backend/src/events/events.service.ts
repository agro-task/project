import { Injectable } from '@nestjs/common';
import { CreateEventInput } from './dto/create-event.input';
import { UpdateEventInput } from './dto/update-event.input';
import { Event } from './entities/event.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Location } from '../locations/entities/location.entity';

@Injectable()
export class EventsService {
  constructor(
    @InjectRepository(Event)
    private eventRepository: Repository<Event>,
  ) {}

  async create(createEventInput: CreateEventInput): Promise<Event> {
    const event = new Event();
    event.name = createEventInput.name;
    event.description = createEventInput.description;
    event.startDate = createEventInput.startDate;
    event.endDate = createEventInput.endDate;

    const location = new Location();
    location.id = createEventInput.locationId;
    event.location = location;

    return this.eventRepository.save(event);
  }

  async findAll(
    startDate: string,
    endDate: string,
    locationId: number,
  ): Promise<Event[]> {
    return this.eventRepository.find({
      where: {
        startDate,
        endDate,
        location: {
          id: locationId,
        },
      },
      relations: ['location'],
    });
  }

  findOne(id: number): Promise<Event | null> {
    return this.eventRepository.findOne({
      where: {
        id,
      },
      relations: ['location'],
    });
  }

  async update(id: number, updateEventInput: UpdateEventInput) {
    const event = await this.findOne(id);

    if (!event) {
      throw new Error('Event not found');
    }

    const result = await this.eventRepository
      .createQueryBuilder()
      .update(Event)
      .set({
        name: updateEventInput.name,
        description: updateEventInput.description,
        startDate: updateEventInput.startDate,
        endDate: updateEventInput.endDate,
        location: {
          id: updateEventInput.locationId,
        },
      })
      .where('id = :id', { id })
      .execute();

    if (result.affected === 0) {
      throw new Error('Event not found');
    }

    return;
  }

  async remove(id: number): Promise<void> {
    const event = await this.findOne(id);

    if (!event) {
      throw new Error('Event not found');
    }

    await this.eventRepository.delete(id);
  }
}
