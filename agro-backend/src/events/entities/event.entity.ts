import { ObjectType, Field, Int } from '@nestjs/graphql';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToOne,
  JoinColumn,
} from 'typeorm';
import { Location } from '../../locations/entities/location.entity';

@ObjectType()
@Entity()
export class Event {
  @Field(() => Int, { description: 'Id of the event' })
  @PrimaryGeneratedColumn()
  id: number;

  @Field(() => String, { description: 'Name of the event' })
  @Column()
  name: string;

  @Field(() => String, { description: 'Description of the event' })
  @Column()
  description: string;

  @Field(() => String, { description: 'Start date of the event' })
  @Column()
  startDate: string;

  @Field(() => String, { description: 'End date of the event' })
  @Column()
  endDate: string;

  @Field(() => Location, {
    description: 'Location of the event',
    nullable: true,
  })
  @ManyToOne(() => Location, (location) => location.id)
  @JoinColumn({
    name: 'location',
    referencedColumnName: 'id',
  })
  location: Location;
}
