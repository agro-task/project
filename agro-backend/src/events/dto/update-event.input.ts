import { CreateEventInput } from './create-event.input';
import { InputType, Field, Int, PartialType } from '@nestjs/graphql';

@InputType()
export class UpdateEventInput extends PartialType(CreateEventInput) {
  @Field(() => Int)
  id: number;

  @Field(() => String, { description: 'Name of the event' })
  name: string;

  @Field(() => String, { description: 'Description of the event' })
  description: string;

  @Field(() => String, { description: 'Start date of the event' })
  startDate: string;

  @Field(() => String, { description: 'End date of the event' })
  endDate: string;

  @Field(() => Int, { description: 'Id of the location' })
  locationId: number;
}
