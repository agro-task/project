import { Injectable } from '@nestjs/common';
import { CreateLocationInput } from './dto/create-location.input';
import { UpdateLocationInput } from './dto/update-location.input';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Location } from './entities/location.entity';
import { DeleteResult } from 'typeorm';

@Injectable()
export class LocationsService {
  constructor(
    @InjectRepository(Location)
    private locationRepository: Repository<Location>,
  ) {}

  create(createLocationInput: CreateLocationInput): Promise<Location> {
    const location = new Location();
    location.name = createLocationInput.name;

    return this.locationRepository.save(location);
  }

  findAll(): Promise<Location[]> {
    return this.locationRepository.find();
  }

  findOne(id: number): Promise<Location | null> {
    return this.locationRepository.findOneBy({
      id,
    });
  }

  async update(
    id: number,
    updateLocationInput: UpdateLocationInput,
  ): Promise<Location> {
    const location = await this.findOne(id);

    if (!location) {
      throw new Error('Location not found');
    }

    location.name = updateLocationInput.name;

    return this.locationRepository.save(location);
  }

  async remove(id: number): Promise<DeleteResult> {
    const location = await this.findOne(id);

    if (!location) {
      throw new Error('Location not found');
    }

    return this.locationRepository.delete(id);
  }
}
