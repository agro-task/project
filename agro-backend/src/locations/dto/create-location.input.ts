import { InputType, Field } from '@nestjs/graphql';

@InputType()
export class CreateLocationInput {
  @Field(() => String, { description: 'Name of the location' })
  name: string;
}
