import { ObjectType, Field, Int } from '@nestjs/graphql';
import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { Event } from '../../events/entities/event.entity';

@ObjectType()
@Entity()
export class Location {
  @Field(() => Int, { description: 'Id of the location' })
  @PrimaryGeneratedColumn()
  id: number;

  @Field(() => String, { description: 'Name of the location' })
  @Column()
  name: string;

  @OneToMany(() => Event, (event) => event.location)
  events: Event[];
}
