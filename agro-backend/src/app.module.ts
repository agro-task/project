import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { DirectiveLocation, GraphQLDirective } from 'graphql';
import { upperDirectiveTransformer } from './common/directives/upper-case.directive';
import { ApolloDriver, ApolloDriverConfig } from '@nestjs/apollo';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EventsModule } from './events/events.module';
import { SocketModule } from './socket/socket.module';
import { LocationsModule } from './locations/locations.module';

@Module({
  imports: [
    GraphQLModule.forRoot<ApolloDriverConfig>({
      driver: ApolloDriver,
      autoSchemaFile: 'schema.gql',
      transformSchema: (schema) => upperDirectiveTransformer(schema, 'upper'),
      installSubscriptionHandlers: true,
      buildSchemaOptions: {
        directives: [
          new GraphQLDirective({
            name: 'upper',
            locations: [DirectiveLocation.FIELD_DEFINITION],
          }),
        ],
      },
    }),
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: 'pom.db.elephantsql.com',
      port: 5432,
      username: 'wqiuihgd',
      password: '8qPdphJiZczx7bXUgcvgwwJyAvX_MhPd',
      database: 'wqiuihgd',
      autoLoadEntities: true,
      synchronize: true,
    }),
    EventsModule,
    SocketModule,
    LocationsModule,
  ],
})
export class AppModule {}
